package resources


import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.os.ExecutableFinder
import static org.apache.commons.lang3.SystemUtils.IS_OS_LINUX
import static org.apache.commons.lang3.SystemUtils.IS_OS_MAC
import static org.apache.commons.lang3.SystemUtils.IS_OS_WINDOWS

driver = {
	def defaultExecutable = new ExecutableFinder().find("chromedriver")
	if (defaultExecutable) {
		new File(defaultExecutable)
	} else {
		new File("drivers").listFiles().findAll {
			!it.name.endsWith(".version")
		}.find {
			if (IS_OS_LINUX) {
				it.name.contains("linux")
				System.setProperty("webdriver.chrome.driver",'drivers/chromedriver-linux-64bit')
			} else if (IS_OS_MAC) {
				it.name.contains("mac")
				System.setProperty("webdriver.chrome.driver",'drivers/chromedriver-mac-64bit')
			} else if (IS_OS_WINDOWS) {
				it.name.contains("windows")
				System.setProperty("webdriver.chrome.driver",'drivers/chromedriver-windows.exe')
			}
		}
	}


	ChromeOptions options = new ChromeOptions();
	if(System.properties.get("browserMode").toString().equalsIgnoreCase("chrome")){
		new ChromeDriver(options)
	}else if (System.properties.get("browserMode").toString().equalsIgnoreCase("headless")){
		options.addArguments("--window-size=1920,1200")
		options.addArguments("--headless")
		new ChromeDriver(options)
	}else{
		new ChromeDriver(options)
	}
}

waiting {
	timeout = 60
	retryInterval = 0.5
}
