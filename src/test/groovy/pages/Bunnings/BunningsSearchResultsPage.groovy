package pages.Bunnings

import geb.error.GebAssertionError
import handler.GlobalParam
import org.openqa.selenium.By
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class BunningsSearchResultsPage extends BasePage {

    private static final Logger logger = LoggerFactory.getLogger(BunningsSearchResultsPage.class)
    static at = { title.equalsIgnoreCase("Search results - Bunnings Australia") }

    static content = {
        searchResults(wait: true) { $(By.xpath("//div[@class= 'totalResults']")) }

    }

    boolean getSearchResults() {
        try {
            String strSearchResults = js.exec(" return document.getElementsByClassName('totalResults')[0].innerText;").toString()
            logger.info(strSearchResults + " Search Input is displayed")
            return true
        } catch (GebAssertionError e) {
            logger.error("Got an error when fetching Case from the page " + e.getLocalizedMessage())
            e.printStackTrace()
            return ""
        } catch (Exception e1) {
            logger.error("Got an exception. Exception is " + e1.getLocalizedMessage())
            e1.printStackTrace()
            return false
        }

    }
}
