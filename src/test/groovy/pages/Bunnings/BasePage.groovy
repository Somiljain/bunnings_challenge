package pages.Bunnings

import geb.Page
import geb.error.GebAssertionError
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class BasePage extends Page{

    private static final Logger logger = LoggerFactory.getLogger(BasePage.class)

    static content={
    }


    boolean clickAction (def objToClick, def fieldDescription, def sleepTime =0) {
        try {
            waitFor (sleepTime) { objToClick.isDisplayed() }
            objToClick.click()
            logger.info("Successfully clicked on the ${fieldDescription}")
            return true
        } catch (GebAssertionError e) {
            logger.error("Got an exception when clicking button. Error is " + e.getLocalizedMessage())
            e.printStackTrace()
            return false
        } catch (Exception e1) {
            logger.error("Got an exception. Exception is " + e1.getLocalizedMessage())
            e1.printStackTrace()
            return false
        }
    }


    def edtField(def edtField, def value, def fieldDescription) {
        try {
            if (!value.equalsIgnoreCase("") || value != null) {
                waitFor { edtField.isDisplayed() }
                edtField.value(value)
                logger.info("${fieldDescription} entered successfully")
                return true
            }
        } catch (GebAssertionError e){
            logger.error("Got an exception when entering $fieldDescription. Error is " + e.getLocalizedMessage(), e)
            return false
        } catch(Exception e1) {
            logger.error("Got an exception. Exception is " + e1.getLocalizedMessage(), e1)
            return false
        }
    }

}
