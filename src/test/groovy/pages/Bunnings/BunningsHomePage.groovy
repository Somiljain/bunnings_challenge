package pages.Bunnings

import handler.GlobalParam
import org.openqa.selenium.By
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class BunningsHomePage extends BasePage {

    private static final Logger logger = LoggerFactory.getLogger(BunningsHomePage.class)
    static url = "https://www.bunnings.com.au/"
    static at = {title.equalsIgnoreCase("Australia DIY, Garden & Hardware Store - Bunnings Australia") }

    static content={
        searchBox (wait:true) { $(By.xpath("//div[@title= 'header-search']//input"))}
        btnSearchIcon (wait:true){ $(By.xpath("//button[@id='crossIcon']"))}
        selSearchResult (wait:true){ $(By.xpath("//span[@data-locator= 'Popular searches-0']"))}
        btnClearSearch (wait:true){ $(By.xpath("//p[contains(text(),'Clear Recent Searches')]"))}
    }

    boolean enterSearchItem(String strSearchInput){
        driver.manage().window().maximize()
        sleep(3000)
        edtField(searchBox,strSearchInput, "$strSearchInput is provided as Search Item")
    }

    boolean clickSearchIcon(){

        clickAction(btnSearchIcon, "Search Icon", 2)
    }

    boolean clickSearchResult(){

        clickAction(selSearchResult, "1st Popluar Search Result", 2)
    }

    boolean clearSearchResult(){

        clickAction(btnClearSearch, "Recent Search Result is cleared", 2)
    }

    boolean handleSearchIcon(){
        String strSearchInput = ""

        if (GlobalParam.reqFields.containsKey("SEARCH_INPUT")) {
            if (!GlobalParam.reqFields.get("SEARCH_INPUT").equalsIgnoreCase("")) {
                strSearchInput = GlobalParam.reqFields.get("SEARCH_INPUT")
            }
            //Call function to search item
            if (!enterSearchItem(strSearchInput)) {
                return false
            }
        }
        String strClearSearch = ""
        if (GlobalParam.reqFields.containsKey("CLEAR_SEARCH")) {
            if (!GlobalParam.reqFields.get("CLEAR_SEARCH").equalsIgnoreCase("")) {
                strClearSearch = GlobalParam.reqFields.get("CLEAR_SEARCH")
            }
        }

        if(strSearchInput.empty && strClearSearch == "N"){
            //Call function to select 1st popular item
            if (!clickSearchResult()) {
                return false
            }
        }
        else if (strSearchInput.empty && strClearSearch == "Y"){
            //Call function to search item
            if (!clearSearchResult()) {
                return false
            }
            if (!enterSearchItem(strSearchInput)) {
                return false
            }
            if (!clickSearchResult()) {
                return false
            }
        }else {
            //Call function to search item
            if (!clickSearchIcon()) {
                return false
            }
        }
    }

}
