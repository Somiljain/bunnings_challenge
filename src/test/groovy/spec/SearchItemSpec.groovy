package spec

import geb.spock.GebReportingSpec
import handler.GlobalParam
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import pages.Bunnings.BunningsHomePage
import pages.Bunnings.BunningsSearchResultsPage
import spock.lang.Stepwise
import spock.lang.Unroll


@Stepwise
class SearchItemSpec extends GebReportingSpec {
    private static final Logger logger = LoggerFactory.getLogger(SearchItemSpec.class)

    def setupSpec() {

    }

    @Unroll
    def "Search Item on Bunnings Page using Search Icon Spec #scenario"() {

        GlobalParam.reqFields.put("SEARCH_INPUT", SEARCH_INPUT)
        GlobalParam.reqFields.put("CLEAR_SEARCH", CLEAR_SEARCH)

        given: "User is on the Bunnings main page"
        BunningsHomePage bunningsHomePage = to BunningsHomePage

        when: "User enters search item and clicks on search icon"
        bunningsHomePage.handleSearchIcon()

        and: "User is redirected to Search Results Page"
        BunningsSearchResultsPage bunningsSearchResultsPage = waitFor { at BunningsSearchResultsPage }

        then: "Search Results Page should be displayed"
        assert bunningsSearchResultsPage.getSearchResults()

        where:
        scenario | SEARCH_INPUT | CLEAR_SEARCH
        "SearchEmpty" | "" | "N"
        "SearchIconInput" | "Blower" | "N"
        "ClearRecentSearch" | "" | "Y"
    }
}
